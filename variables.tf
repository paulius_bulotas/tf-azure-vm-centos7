# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "tf_subscription_id" {
  description = "The Azure subscription ID"
}

variable "tf_tenant_id" {
  description = "The Azure tenant ID"
}

variable "tf_client_id" {
  description = "The Azure client ID"
}

variable "tf_secret_access_key" {
  description = "The Azure secret access key"
}

variable "tf_hostname" {
  description = "hosts name"
}

variable "tf_ip_address" {
  description = "hosts IP address from subnet"
}
variable "tf_os_disk_size" {
  description = "OS disk size"
  default = 10
}

variable "tf_data_disk_size" {
  description = "Data disk size"
  default = 30
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "tf_version" {
  description = "Specifies Terraform script version"
  default     = "0.0.1"
}

variable "tf_ssh_user" {
  description = "SSH user on all our servers"
  default = "devbridge"
}

variable "tf_location" {
  description = "The Azure region the consul cluster will be deployed in"
  default     = "West US"
}

variable "tf_loc" {
  default = "USW"
}

variable "tf_rgname" {
  description = "Resource group name"
}

variable "tf_prefix" {
  description = "Default prefix for naming"
  default     = "DBG-LIV"
}

variable "tf_vm_size" {
  default = "Standard_B2S"
}

variable "tf_subnet_id" {
  description = "subnet for VM in "
  default = "/subscriptions/bc2b9883-2501-4da4-8635-afddf03363c7/resourceGroups/DBG-LIV-USW-RG-NETHUB/providers/Microsoft.Network/virtualNetworks/DBG-LIV-USW-VNET-DFL-01/subnets/DBG-LIV-USW-NET-DFL-PRIVATE-01"
}
