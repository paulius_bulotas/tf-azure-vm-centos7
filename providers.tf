provider "azurerm" {
  subscription_id = "${var.tf_subscription_id}"
  client_id       = "${var.tf_client_id}"
  client_secret   = "${var.tf_secret_access_key}"
  tenant_id       = "${var.tf_tenant_id}"
}
