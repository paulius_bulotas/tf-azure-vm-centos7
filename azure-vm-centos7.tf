resource "azurerm_resource_group" "main" {
  name     = "${var.tf_prefix}-${var.tf_loc}-RG-${var.tf_rgname}"
  location = "West US"
  tags {
    owner = "paulius.bulotas@devbridge.com"
  }
}

resource "azurerm_network_interface" "main" {
  name                = "${var.tf_hostname}-NIC-1"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  dns_servers = ["10.50.5.151", "10.0.0.151", "10.100.5.151"]

  ip_configuration {
    name                          = "ipconfig"
    subnet_id                     = "${var.tf_subnet_id}"
    private_ip_address_allocation = "dynamic"
    #private_ip_address_allocation = "Static"
    #private_ip_address            = "${var.tf_ip_address}"
  }
}

resource "azurerm_virtual_machine" "main" {
  name                  = "${var.tf_prefix}-VM-${upper(var.tf_hostname)}"
  location              = "${azurerm_resource_group.main.location}"
  resource_group_name   = "${azurerm_resource_group.main.name}"
  network_interface_ids = ["${azurerm_network_interface.main.id}"]
  vm_size               = "${var.tf_vm_size}"

  delete_os_disk_on_termination = true
  delete_data_disks_on_termination = false

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${lower(var.tf_hostname)}osdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb = "${var.tf_os_disk_size}"
  }

  storage_data_disk {
    name = "${lower(var.tf_hostname)}datadisk1"
    create_option = "Empty"
    disk_size_gb = "${var.tf_data_disk_size}"
    lun = "1"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.tf_hostname}"
    admin_username = "${var.tf_ssh_user}"
    admin_password = "${file(".password.txt")}"
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys = [{
      path     = "/home/${var.tf_ssh_user}/.ssh/authorized_keys"
      key_data = "${file("~/.ssh/id_rsa.pub")}"
    }]
  }
}
